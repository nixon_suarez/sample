import { useState } from 'react';
// import Form from 'react-bootstrap/Form';
// import Button from 'react-bootstrap/Button';
import { Form, Button } from 'react-bootstrap';
import Router from 'next/router';

export default function create() {
    //declare form input states
    const [courseName, setCourseName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    //function for processing creation of a new course
    function addCourse(e) {
        e.preventDefault();

        fetch('http://localhost:4000/api/courses', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: courseName,
                description: description,
                price: price
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            if (data === true){
                Router.push('/courses')
            } else {
                Router.push('/errors/1')
            }
        })
    }

    return (
        <Form onSubmit={(e) => addCourse(e)}>

            <Form.Group controlId="courseName">
                <Form.Label>Course Name:</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter course name"
                    value={courseName}
                    onChange={e => setCourseName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Course Description:</Form.Label>
                <Form.Control
                    as="textarea"
                    rows="3"
                    placeholder="Enter course description"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="price">
                <Form.Label>Course Price:</Form.Label>
                <Form.Control
                    type="number"
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                />
            </Form.Group>

            <Button className="bg-primary" type="submit">
                Submit
            </Button>
        </Form>
    )
}